package com.nick80835.add

import com.google.gson.internal.LinkedTreeMap

data class TrackData(
    // shared vars
    var primaryName: String? = null,
    var secondaryName: String? = null,
    var tertiaryName: String? = null,
    var coverSmall: String? = null,
    var coverBig: String? = null,
    var coverXL: String? = null,
    var explicit: Boolean? = false,
    var resultRaw: LinkedTreeMap<*, *>? = null,

    // album-only vars
    var genreId: Int? = null,
    var contentListUrl: String? = null,
    var trackCount: Int? = null,

    // track-only vars
    var trackId: Long? = null,
    var trackLength: Int? = null,
    var trackPreviewUrl: String? = null,
    var linkedAlbumId: Long? = null,
    var tagData: TrackTagData? = null,
    var readable: Boolean? = true,

    // app vars
    var cardId: Int = cardIdCounter++,
    var cardType: String? = null
)

data class TrackTagData(
    var trackLyrics: String? = null,
    var trackNumber: Int? = null,
    var trackBPM: Float? = null,
    var trackGenre: String? = null,
    var trackContributors: String? = null,
    var trackDate: String? = null
)

data class DownloadContainer(
    var trackData: TrackData? = null,
    var filePathEncrypted: String? = null,
    var filePathComplete: String? = null,
    var blowfishKey: String? = null,
    var taskId: String? = null,
    var downloadUrl: String? = null
)
